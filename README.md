## Reto Tecnico Frontend - Indra

Este aplicacion se ha construido usando [Next.js](https://nextjs.org/) y [React](https://nextjs.org/) creado con el comando[`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

Tecnologías usadas

- TypeScript
- React
- Next

Se han creado Custom Hooks

- useCustomer ( los datos del cliente )
- useFamiliares ( los familiares de un cliente )

Se han usado los siguientes Provider

- HistoryProvider ( Historia de la navegacion de rutas )
- StoreProvider ( Estado global de la aplicacion)

Se ha crado un HOC basico para el control de acceso a las rutas

- withStepper ( para el control de pasos y rutas )

## Instalacion

Primero, clona este repositorio:

```bash
git clone https://gitlab.com/Chasty1/retoui-rimac.git
# y luego
cd retoui-rimac
```

Segundo, ejecuta este comando para instalar las dependencias.

```bash
npm install
# or
yarn install
```

Finalmente, ejecuta el servidor de desarrollo:

```bash
npm run dev
# or
yarn dev
```

Abre [http://localhost:5000](http://localhost:5000) en el navegador para ver la aplicación web.

## Uso

### Caso Feliz

Se tiene un usuario existente en la base de datos, el cual ingresara con las siguientes credenciales

```
- Nro de Documento    : 25809150
- Fecha de nacimiento : 01/01/2021
- Celular             : 988030228
```

Este llamara a la api proporcionada y traera los datos de la persona.

<img height="400"  src="doc/images/caso-feliz.jpeg">

### Cuando el dni no esta en la bd

En este caso se ingresara cualquier dato correcto diferente proporcionado al de arriba.

Como este no existe en la bd este sera el resultado

<img height="400"  src="doc/images/caso-sin-dni.jpeg">

## Video Demo

[![Watch the video](doc/images/preview.png)](https://www.youtube.com/watch?v=-sWMlUyg27U&feature=youtu.be)
