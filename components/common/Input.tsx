import React from "react";
import styles from "./styles/input.module.css";

type InputProps = {
  id: string;
  type: string;
  placeholder: string;
  iconPath: string;
  name: string;
  value: string;
  onChange: (e) => void;
  onClickIcon?: (e) => void;
};

function Input({
  id,
  type,
  placeholder,
  iconPath,
  name,
  onChange,
  onClickIcon,
  value,
}: InputProps) {
  return (
    <div style={{ position: "relative", width: "100%" }}>
      <input
        type={type}
        name={name}
        value={value}
        onChange={onChange}
        className={styles.inputText}
        required
        style={{ paddingTop: 7, backgroundColor: "white" }}
      />
      <span className={styles.floating__label}>{placeholder}</span>

      {iconPath && <img className={styles.icon} src={iconPath} alt="" />}
    </div>
  );
}

export default Input;
