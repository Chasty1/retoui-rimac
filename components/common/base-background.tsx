import React from "react";
import BeneficioItem from "./beneficio-item";
import HeaderLogo from "./header-logo";
import styles from "./styles/base-background.module.scss";

const beneficios = [
  {
    iconPath: "/assets/icons/ic_shield.png",
    text: "Compralo de manera facil y rapida"
  },

  {
    iconPath: "/assets/icons/ic_mobile.png",
    text: "Cotiza y ompra tu seguro 100% online"
  },
  {
    iconPath: "/assets/icons/ic_money.png",
    text: "Hasta S/ . 12 millones de cobertura anual"
  },
  {
    iconPath: "/assets/icons/ic_clinic.png",
    text: "Mas de 300 clinicas en todo el Peru"
  }
];

export default function BaseBackground({
  hasContent,
  illustration1,
  illustration2
}) {
  return (
    <div className={styles.container}>
      <HeaderLogo />
      <div className={styles.container_content}>
        <div className={styles.container_content_fullWidth}>
          {hasContent && (
            <div className={styles.boxContainer}>
              <h1 className={styles.boxContainer__title}>Seguro de Salud</h1>
              <div className={styles.cotiza}>
                <img
                  className={styles.boxContainer__title_icon}
                  src="/assets/icons/ic_shield.png"
                  alt=""
                />
                <p className={styles.boxContainer__description}>
                  Cotiza y compra tu seguro 100% digital
                </p>

                <div className={styles.arrow__container}>
                  <img src="/assets/icons/ic_back.png" alt="" />
                  <span style={{ color: "white" }}>01</span>
                  <span style={{ color: "#ffffff60" }}>/</span>
                  <span style={{ color: "#ffffff60" }}>02</span>
                  <img src="/assets/icons/ic_next.png" alt="" />
                </div>
              </div>

              <div className={styles.boxContainer__beneficios}>
                {beneficios.map((b, index) => (
                  <BeneficioItem
                    key={index}
                    iconPath={b.iconPath}
                    text={b.text}
                  />
                ))}
              </div>
            </div>
          )}

          <img
            className={styles.login__header__illustration1}
            src={illustration1}
            alt=""
          />

          <img
            className={styles.login__header__illustration2}
            src={illustration2}
            alt=""
          />
        </div>
      </div>
    </div>
  );
}
