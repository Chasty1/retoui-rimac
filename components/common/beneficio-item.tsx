import React from "react";
import styles from "./styles/beneficio-item.module.scss";

type BeneficioItemProps = {
  text: string;
  iconPath: string;
  title?: string;
  underlined?: boolean;
  textColor?: string;
  style?: any;
};

function BeneficioItem({
  text,
  iconPath,
  title,
  underlined,
  textColor,
  style,
}: BeneficioItemProps) {
  return (
    <div className={styles.beneficio__item} style={style}>
      <img className={styles.beneficio__item__icon} src={iconPath} alt="" />
      <div className={styles.beneficio__item__text}>
        {title && <h3>{title}</h3>}
        {title ? (
          <p
            style={{
              textDecoration: underlined ? "line-through" : null,
              color: underlined ? null : "black",
              fontSize: 11,
              marginTop: 5,
            }}
          >
            {text}
          </p>
        ) : (
          <p
            style={{
              textDecoration: underlined ? "line-through" : null,
              color: textColor ? textColor : null,
            }}
          >
            {text}
          </p>
        )}
      </div>
    </div>
  );
}

export default BeneficioItem;
