import React from "react";
import styles from "./styles/button.module.scss";

type ButtonProps = {
  text: string;
  disabled: boolean;
  onClick: () => void;
};

function Button({ disabled, text, onClick }: ButtonProps) {
  return (
    <div className={styles.button}>
      <button
        disabled={disabled}
        onClick={onClick}
        className={[styles.container, disabled ? styles.disabled : ""].join(
          " "
        )}
      >
        {text}
      </button>
    </div>
  );
}

export default Button;
