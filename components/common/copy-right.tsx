import React from "react";
import styles from "./styles/copy-right.module.scss";
function CopyRight() {
  return <p className={styles.copyright}>@2020 RIMAC Seguros y Reaseguros</p>;
}

export default CopyRight;
