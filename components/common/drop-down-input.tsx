import React from "react";
import Input from "./Input";
import styles from "./styles/drop-down-input.module.scss";
function DropDownInput({ children, text }) {
  return (
    <div className={styles.container}>
      <div className={styles.container__dropdown}>
        <span>{text}</span>
        <img src="/assets/icons/chevrot.png" alt="" />
      </div>
      {children}
    </div>
  );
}

export default DropDownInput;
