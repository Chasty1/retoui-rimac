import React from "react";
import styles from "./styles/header-logo.module.scss";
function HeaderLogo() {
  return (
    <div className={styles.container}>
      <img src="/assets/images/logo-rimac.png" />
    </div>
  );
}

export default HeaderLogo;
