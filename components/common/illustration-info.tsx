import React from "react";
import styles from "./styles/illustration-info.module.scss";
import SubTitle from "./sub-title";

function IllustrationInfo({ text }) {
  return (
    <div className={styles.illustration}>
      <div className={styles.illustration__info}>
        <SubTitle>{text}</SubTitle>
        <img
          style={{ marginRight: 8 }}
          src="/assets/icons/chevrot.png"
          alt=""
        />
      </div>

      <div style={{ height: 2, backgroundColor: "#E4E8F7" }}></div>
    </div>
  );
}

export default IllustrationInfo;
