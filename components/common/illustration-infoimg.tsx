import React from "react";
import styles from "./styles/illustration-infoimg.module.scss";
import Title from "./title";

function IllustrationInfoImg() {
  return (
    <div className={styles.illustration__infoimg}>
      <div>
        <Title style={{ fontSize: 16, color: "#333" }}>Revisa nuestros</Title>
        <Title style={{ fontSize: 16 }}>
          <span>servicios y exclusiones</span>
        </Title>
      </div>
      <img src="/assets/images/tio-rimac.png" alt="" />
    </div>
  );
}

export default IllustrationInfoImg;
