import React from "react";
import styles from "./styles/plan-item.module.scss";

type PlanItemProps = {
  plan: string;
  price: string;
  frequency: string;
  selected?: boolean;
  onClick?: () => void;
};

function PlanItem({
  plan,
  price,
  frequency,
  selected,
  onClick,
}: PlanItemProps) {
  return (
    <div
      onClick={onClick}
      className={[styles.plan, selected ? styles.selected : ""].join(" ")}
    >
      <p>{plan}</p>
      <div className={styles.plan__amount}>
        <span className={styles.plan__amount__coin}>S/</span>
        <span className={styles.plan__amount__value}>{price}</span>
        <span className={styles.plan__amount__type}>{frequency}</span>
        {selected && <img src="/assets/icons/gl_correct.png" alt="" />}
      </div>
    </div>
  );
}

export default PlanItem;
