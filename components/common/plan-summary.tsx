import React from "react";
import styles from "./styles/plan-summary.module.scss";
import { useFamiliares } from "../../store/customer";

function PlanSummary() {
  const familiares = useFamiliares();
  return (
    <div className={styles.plan__summary}>
      <span>Tienes ({familiares ? familiares.length : 0}) asegurados</span>
      <span>Resumen del plan</span>
    </div>
  );
}

export default PlanSummary;
