import React from "react";
import { planes } from "../../__mocks__";
import BeneficioItem from "./beneficio-item";
import styles from "./styles/beneficio-card.module.scss";

function Prueba({ plan }) {
  return (
    <div className={styles.card}>
      <div className={styles.card__header}>
        <p>Cuentas con estos beneficios</p>
      </div>

      <div className={styles.card__body}>
        <div className={styles.card__body__title}>
          <div className={styles.card__body__title__description}>
            <p>Cobertura maxima</p>
            <h2>S/ {plan.cobertura}</h2>
            <span>{plan.plan}</span>
          </div>

          <img src="assets/icons/coins.png" alt="" />
        </div>

        <div style={{ height: 1, backgroundColor: "#f0f2fa" }}></div>

        <div>
          {plan.beneficios.map((b, index) => (
            <BeneficioItem
              key={index}
              iconPath={b.iconPath}
              title={b.title}
              text={b.text}
              textColor={b.textColor}
              underlined={b.underlined}
              style={{ padding: "10px 0px" }}
            />
          ))}
        </div>
      </div>
    </div>
  );
}

export default Prueba;
