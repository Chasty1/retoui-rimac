import React from "react";
import styles from "./styles/separator.module.scss";

function Separator({ size }) {
  return <div style={{ marginTop: size }}></div>;
}

export default Separator;
