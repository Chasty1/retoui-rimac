import React from "react";
import styles from "./styles/stepper.module.scss";

type StepperProps = {
  currentStep: number;
  steps: number;
};

function Stepper({ currentStep, steps }: StepperProps) {
  return (
    <div className={styles.stepper}>
      <img src="/assets/icons/ic_round_back.png" alt="" />
      <span className={styles.stepper__stepText}>PASO</span>
      <span className={styles.stepper__currentStep}>{currentStep}</span>
      <div className={styles.stepper__steps}>
        <span>DE</span>
        <span>{steps}</span>
      </div>
    </div>
  );
}

export default Stepper;
