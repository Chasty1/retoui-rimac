import React from "react";
import styles from "./styles/sub-title.module.scss";

function SubTitle({ children }) {
  return <p className={styles.container}>{children}</p>;
}

export default SubTitle;
