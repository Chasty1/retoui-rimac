import React from "react";
import styles from "./styles/term-condition.module.scss";

type TermConditionProps = {
  text: string;
  onChange: (e) => void;
};
function TermCondition({ text, onChange }: TermConditionProps) {
  return (
    <div className={styles.container}>
      <input onChange={onChange} type="checkbox" />
      <p>{text}</p>
    </div>
  );
}

export default TermCondition;
