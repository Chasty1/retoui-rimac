import { truncate } from "fs";
import React, { useReducer, useState } from "react";
import BaseBackground from "./base-background";
import Button from "./button";
import DropDownInput from "./drop-down-input";
import HeaderLogo from "./header-logo";
import Input from "./Input";
import styles from "./styles/wrapper.module.scss";
import SubTitle from "./sub-title";
import TermCondition from "./term-condition";
import Title from "./title";
import { useRouter } from "next/router";
import useCustomer from "../../hooks/useCustomer";
import { clienteExisteEnBD } from "../../utils/functions";

function Login() {
  const [firstTermChecked, setFirstTermChecked] = useState(false);
  const [secondTermChecked, setSecondTermChecked] = useState(false);

  const router = useRouter();

  const [inputValues, setInputValues] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    { celular: "", fechaNacimiento: "", dni: "" }
  );

  const handleOnChange = (event) => {
    const { name, value } = event.target;
    setInputValues({ ...inputValues, [name]: value });
  };

  const { customer, searchCustomer } = useCustomer({ ...inputValues });

  return (
    <div className={styles.login}>
      <BaseBackground
        illustration1="/assets/images/illustration-one.png"
        illustration2="/assets/images/illustration-two.png"
        hasContent={true}
      />
      <div className={styles.login__form}>
        <Title>
          Obten tu <span>seguro ahora</span>
        </Title>

        <SubTitle>ingresa los datos para comenzar</SubTitle>

        <DropDownInput text="DNI">
          <Input
            value={{ ...inputValues }.dni}
            id="dni"
            name="dni"
            iconPath=""
            onChange={(e) => handleOnChange(e)}
            onClickIcon={() => null}
            type="text"
            placeholder="Nro de Documento"
          />
        </DropDownInput>

        <Input
          value={{ ...inputValues }.fechaNacimiento}
          id="fechaNacimiento"
          name="fechaNacimiento"
          iconPath="/assets/icons/ic_calendar.png"
          onChange={(e) => handleOnChange(e)}
          onClickIcon={() => null}
          type="date"
          placeholder="Fecha de nacimiento"
        />

        <Input
          value={{ ...inputValues }.celular}
          id="celular"
          name="celular"
          iconPath=""
          onChange={(e) => handleOnChange(e)}
          onClickIcon={() => null}
          type="text"
          placeholder="Celular"
        />

        <TermCondition
          text="Acepto la Politica de Proteccion de Datos Perosnales y los Terminos
            y Condiciones"
          onChange={(e) => {
            setFirstTermChecked(e.target.checked);
          }}
        />

        <TermCondition
          onChange={(e) => {
            setSecondTermChecked(e.target.checked);
          }}
          text="Acepto la Politica de Envio de Comunicaciones Comerciales"
        />

        <div style={{ marginTop: 20 }}></div>

        <Button
          disabled={!firstTermChecked || !secondTermChecked}
          text={"Comencemos"}
          onClick={() => {
            //router.push("/datos");
            searchCustomer();
          }}
        />

        <p
          style={{
            fontSize: 10,
            marginTop: 30,
            paddingBottom: 20,
            color: "#676f8f"
          }}
        >
          @2020 RIMAC Seguros y Reaseguros
        </p>
      </div>
    </div>
  );
}

export default Login;
