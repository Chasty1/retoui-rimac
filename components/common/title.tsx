import React from "react";
import styles from "./styles/title.module.scss";
type TitleProps = {
  children: any;
  style?: any;
};

function Title({ children, style }: TitleProps) {
  return (
    <h3 className={styles.container} style={style}>
      {children}
    </h3>
  );
}

export default Title;
