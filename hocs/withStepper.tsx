import { useLayoutEffect, FC, useState, useEffect, useMemo } from "react";
import { useRouter } from "next/router";
import { useHistory } from "../contexts/History";

export default function withStepper(ProtectedComponent: FC) {
  return (props) => {
    const [isStepperOk, setIsStepperOk] = useState<boolean>(false);
    const router = useRouter();
    const { history, back } = useHistory();

    useMemo(() => history, []);

    useLayoutEffect(() => {
      if (history.length == 1) {
        const path = history[0];
        if (path !== "/") {
          router.push("/");
        }
      } else {
        setIsStepperOk(true);
      }
    }, [history]);

    return isStepperOk ? <ProtectedComponent {...props} /> : null;
  };
}
