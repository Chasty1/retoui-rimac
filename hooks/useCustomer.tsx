import { useState, useRef, useEffect } from "react";
import { useRouter } from "next/router";
import getCustomer from "../service/get-customer";
import { useCustomer, useSetCustomer } from "../store/customer";
import {
  clienteExisteEnBD,
  validateDoc,
  validatePhone,
} from "../utils/functions";

function useHookCustomer({ dni, fechaNacimiento, celular }) {
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const customer = useCustomer();
  const setCustomer = useSetCustomer();
  const router = useRouter();

  let cancel = useRef(false);

  const searchCustomer = () => {
    // Cancelar muchas request

    if (!validateDoc(dni)) {
      setError("Documento Invalido");
      return;
    }

    if (!fechaNacimiento) {
      setError("Fecha Invalida");
      return;
    }

    if (!validatePhone(celular)) {
      setError("Celular Invalido");
      return;
    }

    if (!clienteExisteEnBD({ dni, fechaNacimiento, celular })) {
      //alert("Cliente no existe en bd");
      setCustomer(null);
      router.push("/datos");
      return;
    }

    setIsLoading(true);
    getCustomer({ dni, fechaNacimiento, celular })
      .then((res) => {
        let customerResponse = res.data.tercero;
        setCustomer(customerResponse);
        setIsLoading(false);
        router.push("/datos");
      })
      .catch((e) => {
        //setError(e.error_description);
        setIsLoading(false);
      })
      .finally(() => {
        //cancel.current = true;
        router.push("/datos");
      });
  };

  return { customer, searchCustomer, isLoading, error };
}

export default useHookCustomer;
