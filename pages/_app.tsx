import "../styles/globals.scss";

import store from "../store";

const { StoreProvider } = store;

import { HistoryProvider } from "../contexts/History";

function MyApp({ Component, pageProps }) {
  const initialState = {
    customer: {
      familiares: [],
    },
  };
  return (
    <StoreProvider initialState={initialState}>
      <HistoryProvider>
        <Component {...pageProps} />
      </HistoryProvider>
    </StoreProvider>
  );
}

export default MyApp;
