import { useRouter } from "next/router";
import Head from "next/head";
import React, { useEffect, useReducer, useState } from "react";
import BaseBackground from "../../components/common/base-background";
import Button from "../../components/common/button";
import DropDownInput from "../../components/common/drop-down-input";
import HeaderLogo from "../../components/common/header-logo";
import Input from "../../components/common/Input";
import Stepper from "../../components/common/stepper";
import SubTitle from "../../components/common/sub-title";
import Title from "../../components/common/title";
import {
  useAddFamiliar,
  useCustomer,
  useDeleteFamiliar,
  useFamiliares
} from "../../store/customer";

import styles from "../../styles/general-style.module.scss";
import { formatDate } from "../../utils/functions";
import withStepper from "../../hocs/withStepper";

function Login() {
  const router = useRouter();

  const [inputValues, setInputValues] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {
      dni: "",
      nombres: "",
      apPaterno: "",
      apMaterno: "",
      fecNacimiento: "",
      fecNacimientoFamilia: ""
    }
  );

  const handleOnChange = (event) => {
    const { name, value } = event.target;
    setInputValues({ ...inputValues, [name]: value });
  };

  const [checkedElement, setCheckedElement] = useState("");
  const [error, setError] = useState("");

  const customer = useCustomer();
  const familiares = useFamiliares();
  const addFamiliar = useAddFamiliar();
  const deleteFamiliar = useDeleteFamiliar();

  useEffect(() => {
    if (customer) {
      setInputValues({
        ...inputValues,
        ["dni"]: customer.numDocumento,
        ["nombres"]: customer.nombres,
        ["fecNacimiento"]: formatDate(customer.fecNacimiento),
        ["apPaterno"]: customer.apellidoPaterno,
        ["apMaterno"]: customer.apellidoMaterno
      });
    }
  }, [customer]);

  return (
    <>
      <Head>
        <title>Datos del cliente</title>
      </Head>
      <div className={styles.wrapper}>
        <div className={styles.mobile}>
          <HeaderLogo />
        </div>

        <div className={styles.not__mobile}>
          <BaseBackground
            illustration1="/assets/images/illustration-one.png"
            illustration2="/assets/images/illustration-two.png"
            hasContent={false}
          />
        </div>

        <div className={styles.wrapper__form}>
          <Stepper currentStep={1} steps={7} />
          <Title>
            Hola{" "}
            <span>{customer.nombres ? customer.nombres : "¡empecemos!"}</span>
          </Title>
          <SubTitle>
            {" "}
            {customer.nombres
              ? `Valida que los datos sean correctos`
              : `Cuéntanos un poco mas de ti`}{" "}
          </SubTitle>
          <div style={{ marginTop: 30 }}>
            <SubTitle>
              {customer.nombres
                ? "Datos personales del titular"
                : "Ingresa tu nombre"}
            </SubTitle>
          </div>
          {customer.nombres && (
            <DropDownInput text="DNI">
              <Input
                value={{ ...inputValues }.dni}
                id="dni"
                name="dni"
                onChange={(e) => handleOnChange(e)}
                iconPath=""
                onClickIcon={() => null}
                type="text"
                placeholder="Nro de Documento"
              />
            </DropDownInput>
          )}
          <Input
            value={{ ...inputValues }.nombres}
            id="nombres"
            name="nombres"
            iconPath=""
            onChange={(e) => handleOnChange(e)}
            onClickIcon={() => null}
            type="text"
            placeholder={customer.nombres ? "Nombres" : "Nombre y Apellido"}
          />
          {customer.nombres && (
            <Input
              id="apPaterno"
              value={{ ...inputValues }.apPaterno}
              name="apPaterno"
              iconPath=""
              onChange={(e) => handleOnChange(e)}
              onClickIcon={() => null}
              type="text"
              placeholder="Apellido Paterno"
            />
          )}
          {customer.nombres && (
            <Input
              id="apMaterno"
              value={{ ...inputValues }.apMaterno}
              name="apMaterno"
              iconPath=""
              onChange={(e) => handleOnChange(e)}
              onClickIcon={() => null}
              type="text"
              placeholder="Apellido Materno"
            />
          )}
          {customer.nombres && (
            <Input
              id="1"
              value={{ ...inputValues }.fecNacimiento}
              name="fecNacimiento"
              iconPath=""
              onChange={(e) => handleOnChange(e)}
              onClickIcon={() => null}
              type="date"
              placeholder="Fecha de Nacimiento"
            />
          )}
          {customer.nombres && (
            <>
              <div style={{ marginTop: 10 }}>
                <SubTitle>Genero</SubTitle>
              </div>

              <div>
                <div style={{ marginTop: 8 }}>
                  <input type="radio" name="gender" value="male" />
                  <label style={{ marginLeft: 10 }} htmlFor="">
                    Masculino
                  </label>
                </div>
                <div style={{ marginTop: 8 }}>
                  <input type="radio" name="gender" value="female" />
                  <label style={{ marginLeft: 10 }} htmlFor="">
                    Femenino
                  </label>
                </div>
              </div>
            </>
          )}
          <div style={{ marginTop: 20, marginBottom: 20 }}>
            <Title style={{ fontSize: 16, color: "#494F66" }}>
              ¿ A quien vamos a asegurar ?
            </Title>
          </div>
          <div>
            <div style={{ marginTop: 8 }}>
              <input
                onChange={(e) => {
                  setCheckedElement(e.target.value);
                }}
                name="who"
                type="radio"
                value="solo"
              />
              <label style={{ marginLeft: 10 }} htmlFor="">
                Solo Ami
              </label>
            </div>
            <div style={{ marginTop: 8 }}>
              <input
                onChange={(e) => {
                  setCheckedElement(e.target.value);
                }}
                name="who"
                type="radio"
                value="familia"
              />
              <label style={{ marginLeft: 10 }} htmlFor="">
                A mi y ami familia
              </label>
            </div>
          </div>
          {checkedElement == "familia" && (
            <>
              <div style={{ marginTop: 20 }}>
                <SubTitle>Datos de tus familiares</SubTitle>
              </div>

              <p style={{ color: "red", fontSize: 12, marginBottom: 12 }}>
                {error}
              </p>

              <DropDownInput text="Conyuge">
                <Input
                  value={{ ...inputValues }.fecNacimientoFamilia}
                  id="fecNacimientoFamilia"
                  name="fecNacimientoFamilia"
                  iconPath=""
                  onChange={(e) => handleOnChange(e)}
                  onClickIcon={() => null}
                  type="date"
                  placeholder="F. Nacimiento"
                />
                <div
                  style={{
                    width: 100,
                    height: "100%",
                    backgroundColor: "transparent",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <img
                    src="/assets/icons/ic_plus.png"
                    alt=""
                    onClick={() => {
                      const fecha = { ...inputValues }.fecNacimientoFamilia;
                      if (fecha) {
                        addFamiliar({
                          conyuge: "Conyuge",
                          fecNacimient: fecha
                        });
                      } else {
                        setError("Fecha Invalida");
                      }
                    }}
                  />
                </div>
              </DropDownInput>

              {familiares?.map((f, index) => (
                <div
                  key={index}
                  style={{
                    padding: "10px 8px",
                    display: "flex",
                    justifyContent: "space-between"
                  }}
                >
                  <span>{f.conyuge}</span>
                  <span>{f.fecNacimient}</span>
                  <img
                    onClick={() => {
                      deleteFamiliar(index);
                    }}
                    src="/assets/icons/ic_delete.png"
                    alt=""
                  />
                </div>
              ))}
            </>
          )}

          <Button
            disabled={false}
            text={"Continuar"}
            onClick={() => {
              router.push("/elige-tu-plan");
            }}
          />
          <div style={{ height: 50, backgroundColor: "transparent" }}></div>
        </div>
      </div>
    </>
  );
}

export default withStepper(Login);
