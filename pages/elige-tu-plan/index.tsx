import { truncate } from "fs";
import { useRouter } from "next/router";
import React, { useState } from "react";
import BaseBackground from "../../components/common/base-background";
import Button from "../../components/common/button";
import HeaderLogo from "../../components/common/header-logo";
import PlanItem from "../../components/common/plan-item";
import Beneficios from "../../components/common/prueba";
import Stepper from "../../components/common/stepper";
import SubTitle from "../../components/common/sub-title";
import Title from "../../components/common/title";
import Head from "next/head";

import styles from "../../styles/general-style.module.scss";
import { planes } from "../../__mocks__";
import withStepper from "../../hocs/withStepper";
import IllustrationInfo from "../../components/common/illustration-info";
import PlanSummary from "../../components/common/plan-summary";
import IllustrationInfoImg from "../../components/common/illustration-infoimg";
import Separator from "../../components/common/separator";

function Login() {
  const [selectedIndexPlan, setSelectedIndexPlan] = useState(0);

  const router = useRouter();

  return (
    <>
      <Head>
        <title>Elige tu protección</title>
      </Head>
      <div className={styles.wrapper}>
        <div className={styles.mobile}>
          <HeaderLogo />
        </div>

        <div className={styles.not__mobile}>
          <BaseBackground
            illustration1="/assets/images/illustration-one.png"
            illustration2="/assets/images/illustration-two.png"
            hasContent={false}
          />
        </div>

        <div className={styles.wrapper__form}>
          <Stepper currentStep={2} steps={7} />

          <Title>
            Elige <span>tu proteccion</span>
          </Title>

          <SubTitle>Selecciona tu plan de salud ideal</SubTitle>

          <div style={{ marginTop: 30 }}></div>

          <div className={styles.carrousel}>
            {planes.map((p, index) => (
              <React.Fragment key={index}>
                <PlanItem
                  selected={index == selectedIndexPlan}
                  plan={p.plan}
                  price={p.price}
                  frequency={p.frequency}
                  onClick={() => setSelectedIndexPlan(index)}
                />
                <div className={styles.plan__item__separator}></div>
              </React.Fragment>
            ))}
          </div>

          <Separator size={20} />

          <PlanSummary />

          <Separator size={20} />

          <Beneficios plan={planes[selectedIndexPlan]} />

          <Separator size={20} />

          <IllustrationInfoImg />

          <Separator size={10} />

          <IllustrationInfo text="Servicios brindados" />

          <IllustrationInfo text="Exclusion" />

          <Separator size={20} />

          <Title
            style={{ fontSize: 12, textAlign: "center", color: "#939DFF" }}
          >
            ENVIAR COTIZACION POR CORRECO
          </Title>

          <Separator size={20} />

          <Button
            onClick={() => {
              router.push("/gracias");
            }}
            disabled={false}
            text={"Comprar Plan"}
          />

          <div style={{ height: 50 }}></div>
        </div>
      </div>
    </>
  );
}

export default withStepper(Login);
