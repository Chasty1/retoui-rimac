import { truncate } from "fs";
import React, { useState } from "react";
import BaseBackground from "../../components/common/base-background";
import Button from "../../components/common/button";
import SubTitle from "../../components/common/sub-title";
import Title from "../../components/common/title";
import styles from "../../components/common/styles/wrapper.module.scss";
import { useRouter } from "next/router";
import withStepper from "../../hocs/withStepper";

function Gracias() {
  const router = useRouter();

  return (
    <div className={styles.wrapper}>
      <BaseBackground
        illustration1="/assets/images/rimac_gracias.png"
        illustration2="/assets/images/illustration-two.png"
        hasContent={false}
      />

      <div className={styles.wrapper__form}>
        <div style={{ marginTop: 30 }}></div>
        <div>
          <div className={styles.image__wrapper}>
            <img
              className={styles.rimac__gracias__desktop}
              src="/assets/images/rimac_gracias.png"
              alt=""
            />
          </div>
          <Title>
            ¡Gracias por <span>confiar en nosostros!</span>
          </Title>
        </div>

        <div style={{ marginTop: 20 }}></div>

        <SubTitle>
          Queremos conocer mejor la salud de los asegurados. Un asesor se pondrá
          en contacto contigo en las siguientes 48 horas.
        </SubTitle>

        <div style={{ marginTop: 20 }}></div>

        <Button
          onClick={() => {
            router.push("/");
          }}
          disabled={false}
          text={"IR A SALUD RIMAC"}
        />
      </div>
    </div>
  );
}

export default withStepper(Gracias);
