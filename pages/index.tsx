import React, { useReducer, useState } from "react";
import { useRouter } from "next/router";
import Head from "next/head";
import useCustomer from "../hooks/useCustomer";
import BaseBackground from "../components/common/base-background";
import styles from "../components/common/styles/wrapper.module.scss";
import Title from "../components/common/title";
import SubTitle from "../components/common/sub-title";
import DropDownInput from "../components/common/drop-down-input";
import Input from "../components/common/Input";
import TermCondition from "../components/common/term-condition";
import Button from "../components/common/button";
import withStepper from "../hocs/withStepper";
import CopyRight from "../components/common/copy-right";

function Login() {
  const [firstTermChecked, setFirstTermChecked] = useState(false);
  const [secondTermChecked, setSecondTermChecked] = useState(false);

  const [inputValues, setInputValues] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    { celular: "", fechaNacimiento: "", dni: "" }
  );

  const handleOnChange = (event) => {
    const { name, value } = event.target;
    setInputValues({ ...inputValues, [name]: value });
  };

  const { customer, searchCustomer, error } = useCustomer({ ...inputValues });

  return (
    <>
      <Head>
        <title>Obten tu seguro - RIMAC</title>
      </Head>
      <div className={styles.wrapper}>
        <BaseBackground
          illustration1="/assets/images/illustration-one.png"
          illustration2="/assets/images/illustration-two.png"
          hasContent={true}
        />
        <div className={styles.wrapper__form}>
          <Title>
            Obten tu <span>seguro ahora</span>
          </Title>

          <SubTitle>ingresa los datos para comenzar</SubTitle>

          <DropDownInput text="DNI">
            <Input
              value={{ ...inputValues }.dni}
              id="dni"
              name="dni"
              iconPath=""
              onChange={(e) => handleOnChange(e)}
              onClickIcon={() => null}
              type="text"
              placeholder="Nro de Documento"
            />
          </DropDownInput>

          <Input
            value={{ ...inputValues }.fechaNacimiento}
            id="fechaNacimiento"
            name="fechaNacimiento"
            iconPath="/assets/icons/ic_calendar.png"
            onChange={(e) => handleOnChange(e)}
            onClickIcon={() => null}
            type="date"
            placeholder="Fecha de nacimiento"
          />

          <Input
            value={{ ...inputValues }.celular}
            id="celular"
            name="celular"
            iconPath=""
            onChange={(e) => handleOnChange(e)}
            onClickIcon={() => null}
            type="text"
            placeholder="Celular"
          />

          <p style={{ color: "red", fontSize: 12 }}>{error}</p>

          <TermCondition
            text="Acepto la Politica de Proteccion de Datos Perosnales y los Terminos
            y Condiciones"
            onChange={(e) => {
              setFirstTermChecked(e.target.checked);
            }}
          />

          <TermCondition
            onChange={(e) => {
              setSecondTermChecked(e.target.checked);
            }}
            text="Acepto la Politica de Envio de Comunicaciones Comerciales"
          />

          <div style={{ marginTop: 20 }}></div>

          <Button
            disabled={!firstTermChecked || !secondTermChecked}
            text={"Comencemos"}
            onClick={() => {
              searchCustomer();
            }}
          />

          <CopyRight />
        </div>
      </div>
    </>
  );
}

export default withStepper(Login);
