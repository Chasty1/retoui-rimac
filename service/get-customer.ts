import { REFUSED } from "dns";
import { ICustomer, IDataReponseApi } from "../store/types";
import { endpoint } from "../utils/consts";

type Params = {
  dni: string;
  fechaNacimiento: string;
  celular: string;
};

export const _withHeaders = (data: Params) => {
  return {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json",
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
  };
};

export default async function getCustomer({
  dni,
  fechaNacimiento,
  celular,
}: Params): Promise<IDataReponseApi> {
  let fail: boolean = true;
  return window
    .fetch(
      `${endpoint.API_RFP_AUTH}/dummy/obtenerdatospersona`,
      _withHeaders({ dni, fechaNacimiento, celular })
    )
    .then((res) => {
      if (!res.ok) {
        fail = false;
      }
      return res.json();
    })
    .then((resJson) => {
      if (!fail) {
        throw resJson;
      }

      return resJson as Promise<IDataReponseApi>;
    })
    .catch((e) => {
      throw e;
    });
}
