import { ICustomer } from "./types";
import customerReducer from "./customer/reducer";

type StateType = {
  customer: ICustomer;
};

const mainReducer = ({ customer }: StateType, action: any) => ({
  customer: customerReducer(customer, action),
});

export default mainReducer;
