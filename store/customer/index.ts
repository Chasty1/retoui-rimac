import { Action, ICustomer, ICustomerFamiliar } from "../types";
import store from "../../store";
import { useEffect, useRef } from "react";

export function useCustomer() {
  const {
    state: { customer },
  } = store.useStore();
  return customer;
}

export function useFamiliares() {
  const {
    state: {
      customer: { familiares },
    },
  } = store.useStore();

  return familiares;
}

export function useSetCustomer() {
  const { dispatch } = store.useStore();

  return (customer: ICustomer) =>
    dispatch({
      type: Action.SetCustomer,
      payload: customer,
    });
}

export function useAddFamiliar() {
  const { dispatch } = store.useStore();

  return (familiar: ICustomerFamiliar) =>
    dispatch({
      type: Action.AddFamiliar,
      payload: familiar,
    });
}

export function useDeleteFamiliar() {
  const { dispatch } = store.useStore();

  return (index: number) =>
    dispatch({
      type: Action.DeleteFamiliar,
      payload: index,
    });
}
