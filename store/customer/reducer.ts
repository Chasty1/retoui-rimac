import { Action, ICustomer, ICustomerFamiliar } from "../types";

const deleteFamiliar = (familiares: ICustomerFamiliar[], indice: number) => {
  const newArray: ICustomerFamiliar[] = [];
  familiares.forEach((element, index) => {
    if (index != indice) newArray.push(element);
  });
  return newArray;
};

const userReducer = (state: ICustomer, action: any) => {
  switch (action.type) {
    case Action.SetCustomer:
      return { ...action.payload };

    case Action.AddFamiliar:
      let newArr = state.familiares;
      if (!state.familiares) {
        newArr = [];
      }

      newArr.push(action.payload);
      return {
        ...state,
        familiares: newArr,
      };

    case Action.DeleteFamiliar:
      console.log(action.payload);
      return {
        ...state,
        familiares: deleteFamiliar(state.familiares, action.payload),
      };

    default:
      return state;
  }
};

export default userReducer;
