import React, { createContext, useReducer, useContext, useMemo } from "react";
import reducer from "./combine-reducers";
import { ICustomer } from "./types";

type IStateType = {
  customer: ICustomer;
};

type IContext = {
  state: IStateType;
  dispatch: React.Dispatch<any>;
};

interface IInitialState {
  initialState: IStateType;
}

export default function makeStore() {
  const context = createContext<IContext>({
    state: { customer: { familiares: [] } },
    dispatch: () => null,
  });

  const StoreProvider: React.FC<IInitialState> = ({
    children,
    initialState,
  }) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const contextValues = useMemo(() => ({ state, dispatch }), [state]);

    return (
      <context.Provider value={contextValues}>{children}</context.Provider>
    );
  };

  const useStore = () => useContext(context);

  return { useStore, StoreProvider };
}
