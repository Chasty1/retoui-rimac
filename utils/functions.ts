import { clientes } from "../__mocks__";

export const clienteExisteEnBD = ({ celular, fechaNacimiento, dni }) => {
  return clientes.find(
    (x) =>
      x.celular == celular &&
      x.fecNacimiento == fechaNacimiento &&
      x.numDocumento == x.numDocumento
  );
};

export const formatDate = (date) => {
  if (!date) return "";
  let temp = date.split("/");
  let newDate = temp[2] + "-" + temp[1] + "-" + temp[0];
  return newDate;
};

export const validatePhone = (phone: string) => {
  return phone.length == 9;
};

export const validateDoc = (doc: string) => {
  return doc.length == 8;
};
